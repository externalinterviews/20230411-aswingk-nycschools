package com.nyc.a20230411_aswingk_nycschools.model.responses

import com.google.gson.annotations.SerializedName

data class School(

    @SerializedName("dbn")
    val id: String,

    @SerializedName("school_name")
    val name :String,

    @SerializedName("phone_number")
    val phone: String,

    val location: String,

    @SerializedName("school_email")
    val email: String,
)