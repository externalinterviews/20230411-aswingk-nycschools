package com.nyc.a20230411_aswingk_nycschools.model.responses

import com.google.gson.annotations.SerializedName

data class SchoolSATScore(

    @SerializedName("dbn")
    val id: String,

    @SerializedName("school_name")
    val name: String,

    @SerializedName("num_of_sat_test_takers")
    val testTakers: String,

    @SerializedName("sat_critical_reading_avg_score")
    val readingAvgScore: String,

    @SerializedName("sat_math_avg_score")
    val mathAvgScore: String,

    @SerializedName("sat_writing_avg_score")
    val writingAvgScore: String
)
