package com.nyc.a20230411_aswingk_nycschools.school

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230411_aswingk_nycschools.databinding.SchoolRecyclerItemBinding
import com.nyc.a20230411_aswingk_nycschools.model.responses.School

class SchoolAdapter : ListAdapter<School, SchoolAdapter.SchoolViewHolder>(DIFF_UTIL_SCHOOLS) {

    interface onClickListener {
        fun onClick(schoolInfo : School)
    }

    lateinit var clickListener: onClickListener

    lateinit var binding: SchoolRecyclerItemBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        binding = SchoolRecyclerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        val item = getItem(position)
        holder.name.setText(item.name)
        holder.phone.setText(item.phone)
        holder.email.setText(item.email)
        holder.location.setText(item.location)
    }

    inner class SchoolViewHolder(itemView: SchoolRecyclerItemBinding) : RecyclerView.ViewHolder(itemView.root){
        val name = itemView.schoolName
        val phone = itemView.schoolPhone
        val email = itemView.schoolEmail
        val location = itemView.schoolLocation
        init {
            itemView.root.setOnClickListener({
                val item = getItem(layoutPosition)
                clickListener.onClick(item)
            })
        }
    }
}