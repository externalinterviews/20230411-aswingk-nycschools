package com.nyc.a20230411_aswingk_nycschools.school

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nyc.a20230411_aswingk_nycschools.model.responses.School
import com.nyc.a20230411_aswingk_nycschools.model.responses.SchoolSATScore
import com.nyc.a20230411_aswingk_nycschools.network.Resource
import com.nyc.a20230411_aswingk_nycschools.repository.SchoolRepository
import kotlinx.coroutines.launch

class SchoolsViewModel(private val repo : SchoolRepository) : ViewModel(){

    private val _schoolResponse : MutableLiveData<Resource<List<School>>> = MutableLiveData()
    val schoolResponse : LiveData<Resource<List<School>>>
        get() = _schoolResponse

    private var _schoolDetailResponse : MutableLiveData<Resource<List<SchoolSATScore>>> = MutableLiveData()
    val schoolDetailResponse : LiveData<Resource<List<SchoolSATScore>>>
        get() = _schoolDetailResponse

    lateinit var selectedSchool : School

    init {
        fetchOrReloadSchoolsList()
    }

    fun fetchOrReloadSchoolsList(){
        viewModelScope.launch {
            _schoolResponse.value = repo.getSchoolsListInfo()
        }
    }

    fun updateSchoolSelection(school: School){
        selectedSchool = school
        _schoolDetailResponse = MutableLiveData()
        viewModelScope.launch {
            _schoolDetailResponse.value = repo.getSchoolDetails(school.id)
        }
    }
}