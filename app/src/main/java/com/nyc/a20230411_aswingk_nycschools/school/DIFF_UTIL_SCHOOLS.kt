package com.nyc.a20230411_aswingk_nycschools.school

import androidx.recyclerview.widget.DiffUtil
import com.nyc.a20230411_aswingk_nycschools.model.responses.School

object DIFF_UTIL_SCHOOLS : DiffUtil.ItemCallback<School>() {
    override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
        return oldItem == newItem
    }
}