package com.nyc.a20230411_aswingk_nycschools.school

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20230411_aswingk_nycschools.R
import com.example.a20230411_aswingk_nycschools.databinding.SchoolsInfoBinding
import com.nyc.a20230411_aswingk_nycschools.base.AppUtils
import com.nyc.a20230411_aswingk_nycschools.network.SchoolApi
import com.nyc.a20230411_aswingk_nycschools.repository.SchoolRepository
import com.nyc.a20230411_aswingk_nycschools.base.BaseFragment
import com.nyc.a20230411_aswingk_nycschools.model.responses.School
import com.nyc.a20230411_aswingk_nycschools.network.Resource

class NYCSchoolsFragment : BaseFragment<SchoolsViewModel, SchoolsInfoBinding, SchoolRepository>() {
    override fun getViewModel(): Class<SchoolsViewModel> = SchoolsViewModel::class.java

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): SchoolsInfoBinding = SchoolsInfoBinding.inflate(inflater, container, false)

    override fun getRepo(): SchoolRepository = SchoolRepository(remoteDataSource.buildSchoolApi(SchoolApi::class.java))

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val itemAdapter = SchoolAdapter()
        with(binding.schoolInfoList){
            layoutManager = LinearLayoutManager(context)
            adapter = itemAdapter
        }

        itemAdapter.clickListener = object : SchoolAdapter.onClickListener{
            override fun onClick(schoolInfo: School) {
                // update the viewModel to retain the reference of School for populating in Details Fragment
                viewModel.updateSchoolSelection(schoolInfo)
                findNavController().navigate(R.id.action_NYCSchoolsFragment_to_schoolDetailsFragment)
            }
        }

        viewModel.schoolResponse.observe(viewLifecycleOwner, {
            when(it){
                is Resource.Success -> {
                    // Sorting the results by name field
                    val data = it.value.sortedBy {
                        it.name
                    }
                    itemAdapter.submitList(data)
                }
                is Resource.Failure -> AppUtils.showToastWithMsg(view.context, AppUtils.getMsgBasedOnNetwork(it.isNetworkError))
            }
            binding.progressBar.visibility = View.GONE
        })
    }
}