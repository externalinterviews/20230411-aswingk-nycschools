package com.nyc.a20230411_aswingk_nycschools.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.a20230411_aswingk_nycschools.R
import com.example.a20230411_aswingk_nycschools.databinding.ActivityMainBinding
import com.nyc.a20230411_aswingk_nycschools.school.SchoolsViewModel
import com.nyc.a20230411_aswingk_nycschools.school_details.SchoolDetailsFragment

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.fab.setOnClickListener { view ->
            fetchOrReloadSchoolsList()
        }

        navController.addOnDestinationChangedListener({ _, dest, _ ->
            when ((dest as FragmentNavigator.Destination).className) {
                SchoolDetailsFragment::class.qualifiedName -> {
                    binding.fab.visibility = View.GONE
                }
                else -> {
                    binding.fab.visibility = View.VISIBLE
                }
            }
        })
    }

    fun fetchOrReloadSchoolsList() {
        val viewModel = ViewModelProvider(this).get(SchoolsViewModel::class.java)
        viewModel.fetchOrReloadSchoolsList()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }
}