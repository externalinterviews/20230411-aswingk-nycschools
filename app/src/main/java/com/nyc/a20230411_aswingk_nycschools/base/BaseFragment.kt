package com.nyc.a20230411_aswingk_nycschools.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.nyc.a20230411_aswingk_nycschools.network.RemoteDataSource

abstract class BaseFragment<VM: ViewModel, VB: ViewBinding, BR: BaseRepository> : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = getBinding(inflater, container)
        val factory = ViewModelFactory(getRepo())
        viewModel = ViewModelProvider(requireActivity(), factory).get(getViewModel())
        return binding.root
    }

    protected val remoteDataSource = RemoteDataSource()

    protected lateinit var binding : VB
    protected lateinit var viewModel : VM

    abstract fun getViewModel() : Class<VM>
    abstract fun getBinding(inflater: LayoutInflater, container: ViewGroup?): VB
    abstract fun getRepo() : BR

}