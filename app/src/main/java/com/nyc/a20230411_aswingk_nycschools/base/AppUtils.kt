package com.nyc.a20230411_aswingk_nycschools.base

import android.content.Context
import android.widget.Toast
import com.example.a20230411_aswingk_nycschools.R

object AppUtils {
    fun showToastWithMsg(context: Context, resourceId: Int, length: Int = Toast.LENGTH_LONG){
        Toast.makeText(context, resourceId, length)
            .show()
    }

    fun getMsgBasedOnNetwork(isNetworkError: Boolean) : Int{
        var resource = 0
        if(isNetworkError) resource = R.string.no_internet_available
        else resource = R.string.unable_to_fetch_school_details
        return resource
    }
}