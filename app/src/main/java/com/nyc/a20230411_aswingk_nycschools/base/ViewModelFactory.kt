package com.nyc.a20230411_aswingk_nycschools.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nyc.a20230411_aswingk_nycschools.repository.SchoolRepository
import com.nyc.a20230411_aswingk_nycschools.school.SchoolsViewModel

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(
    private val repository : BaseRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when{
            modelClass.isAssignableFrom(SchoolsViewModel::class.java) -> SchoolsViewModel(repository as SchoolRepository) as T
            else -> throw IllegalArgumentException("ViewModel Class Not Found")
        }
    }
}