package com.nyc.a20230411_aswingk_nycschools.base

import com.nyc.a20230411_aswingk_nycschools.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException

abstract class BaseRepository {
    suspend fun<T> makeApiCall(apiCall : suspend () -> T) : Resource<T> {

        return withContext(Dispatchers.IO) {
            try {
                    Resource.Success(apiCall.invoke())
                } catch (throwable: Throwable) {
                when (throwable) {
                    is HttpException -> {
                        Resource.Failure(
                            false,
                            throwable.code(),
                            throwable.response()?.errorBody()
                        )
                    }
                    else -> {
                        Resource.Failure(true, null, null)
                    }
                }
            }
        }
    }
}