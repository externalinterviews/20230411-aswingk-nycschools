package com.nyc.a20230411_aswingk_nycschools.repository

import com.nyc.a20230411_aswingk_nycschools.base.BaseRepository
import com.nyc.a20230411_aswingk_nycschools.model.responses.School
import com.nyc.a20230411_aswingk_nycschools.model.responses.SchoolSATScore
import com.nyc.a20230411_aswingk_nycschools.network.Resource
import com.nyc.a20230411_aswingk_nycschools.network.SchoolApi

class SchoolRepository(private val api: SchoolApi) : BaseRepository() {

    suspend fun getSchoolsListInfo(): Resource<List<School>> {
         return makeApiCall { api.getListOfSchoolsInfo() }
    }

    suspend fun getSchoolDetails(id: String): Resource<List<SchoolSATScore>>{
        return makeApiCall { api.getSchoolSATDetails(id) }
    }
}