package com.nyc.a20230411_aswingk_nycschools.school_details

import android.os.Bundle
import android.util.Log
import android.view.*
import com.example.a20230411_aswingk_nycschools.R
import com.example.a20230411_aswingk_nycschools.databinding.SchoolSatDetailsBinding
import com.nyc.a20230411_aswingk_nycschools.base.AppUtils
import com.nyc.a20230411_aswingk_nycschools.base.BaseFragment
import com.nyc.a20230411_aswingk_nycschools.model.responses.School
import com.nyc.a20230411_aswingk_nycschools.model.responses.SchoolSATScore
import com.nyc.a20230411_aswingk_nycschools.network.Resource
import com.nyc.a20230411_aswingk_nycschools.network.SchoolApi
import com.nyc.a20230411_aswingk_nycschools.repository.SchoolRepository
import com.nyc.a20230411_aswingk_nycschools.school.SchoolsViewModel


class SchoolDetailsFragment : BaseFragment<SchoolsViewModel, SchoolSatDetailsBinding, SchoolRepository>() {
    override fun getViewModel(): Class<SchoolsViewModel> = SchoolsViewModel::class.java

    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): SchoolSatDetailsBinding = SchoolSatDetailsBinding.inflate(layoutInflater, container, false)

    override fun getRepo(): SchoolRepository = SchoolRepository(remoteDataSource.buildSchoolApi(SchoolApi::class.java))

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.schoolDetailResponse.observe(viewLifecycleOwner, {
            Log.d("SchoolDetails", "onViewCreated: got the data ${it}")
            when(it){
                is Resource.Success -> {
                    val data = it.value
                    if(!data.isEmpty()) {
                        val schoolSatScore = data.first()
                        updateUI(viewModel.selectedSchool, schoolSatScore)
                    } else {
                        AppUtils.showToastWithMsg(view.context, R.string.unable_to_fetch_school_details)
                    }
                }
                is Resource.Failure -> {
                    AppUtils.showToastWithMsg(view.context, AppUtils.getMsgBasedOnNetwork(it.isNetworkError))
                }
            }
            binding.progressBar.visibility = View.GONE
        })
    }

    fun updateUI(school: School, schoolSATScore: SchoolSATScore){
        binding.schoolName.text = schoolSATScore.name
        binding.schoolPhone.text = school.phone
        updateLocation(school)
        updateEmail(school)

        binding.mathAvgScore.text = resources.getString(R.string.average_math, schoolSATScore.mathAvgScore)
        binding.writingAvgScore.text = resources.getString(R.string.average_writing, schoolSATScore.writingAvgScore)
        binding.readingAvg.text = resources.getString(R.string.average_reading, schoolSATScore.readingAvgScore)
        binding.testTakers.text = resources.getString(R.string.total_test_takers, schoolSATScore.testTakers)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item: MenuItem = menu.findItem(R.id.action_refresh)
        if (item != null)
            item.setVisible(false)
    }

    fun updateLocation(school: School){
        if(school.location.isNullOrEmpty()){
            return
        }
        val location = school.location.substring(0, school.location.indexOf("("))
        binding.location.text = location
    }

    fun updateEmail(school: School){
        if(!school.email.isNullOrEmpty()){
            binding.email.text = school.email
        }
        binding.emailThumb.visibility = if(school.email.isNullOrEmpty()) View.GONE else View.VISIBLE
    }
}