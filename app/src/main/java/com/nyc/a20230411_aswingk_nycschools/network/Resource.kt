package com.nyc.a20230411_aswingk_nycschools.network

import okhttp3.ResponseBody

sealed class Resource<out T> {
    data class Success<out T>(val value: T) : Resource<T>()
    data class Failure(
        val isNetworkError : Boolean,
        val networkErrorCode : Int? = null,
        val networkErrorBody : ResponseBody? = null
    ) : Resource<Nothing>()
}