package com.nyc.a20230411_aswingk_nycschools.network

import com.nyc.a20230411_aswingk_nycschools.model.responses.School
import com.nyc.a20230411_aswingk_nycschools.model.responses.SchoolSATScore
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolApi {
    @GET("/resource/s3k6-pzi2.json")
    suspend fun getListOfSchoolsInfo() : List<School>

    @GET("/resource/f9bf-2cp4.json")
    suspend fun getSchoolSATDetails(
        @Query("dbn") id: String): List<SchoolSATScore>
}