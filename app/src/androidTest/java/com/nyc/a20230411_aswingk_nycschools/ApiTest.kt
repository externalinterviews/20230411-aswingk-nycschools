package com.nyc.a20230411_aswingk_nycschools

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.a20230411_aswingk_nycschools.R
import com.nyc.a20230411_aswingk_nycschools.base.AppUtils
import com.nyc.a20230411_aswingk_nycschools.network.RemoteDataSource
import com.nyc.a20230411_aswingk_nycschools.network.Resource
import com.nyc.a20230411_aswingk_nycschools.network.SchoolApi
import com.nyc.a20230411_aswingk_nycschools.repository.SchoolRepository
import kotlinx.coroutines.runBlocking

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ApiTest {
    @Test
    fun getSchoolsListApiTest() = runBlocking {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.a20230411_aswingk_nycschools", appContext.packageName)

        val remoteDataSource = RemoteDataSource()
        val repository = SchoolRepository(remoteDataSource.buildSchoolApi(SchoolApi::class.java))

        val schoolsList = repository.getSchoolsListInfo()
        when(schoolsList){
            is Resource.Success -> {
                assertTrue(schoolsList.value.size>0)
            }
            is Resource.Failure -> {
                if(schoolsList.isNetworkError){
                    assertTrue(AppUtils.getMsgBasedOnNetwork(schoolsList.isNetworkError) == R.string.no_internet_available)
                } else {
                    assertTrue(AppUtils.getMsgBasedOnNetwork(schoolsList.isNetworkError) == R.string.unable_to_fetch_school_details)
                }
            }
        }
    }
}